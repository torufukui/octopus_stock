package yahoo

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/sclevine/agouti"
	"gitlab.com/torufukui/octopus_stock"
)

// Yahoo画像検索でfetchしてきたデータ情報を格納する構造体
type YahooImageData struct {
	URL                       []string // 取得先のURL
	EncodeStrings             []string // Base64でエンコードされたデータを保存する
	*octopus_stock.StockImage          // 画像関連の情報を保持する
}

// New creates a new YahooImageData
func New(o *octopus_stock.StockImage) *YahooImageData {
	return &YahooImageData{
		StockImage: o,
	}
}

func (g *YahooImageData) ImageStock() {
	g.fetchImageURLOrData()
	g.fetchCreateImage()
}

// Yahoo画像検索で指定したキーワードを元に画像データのURLを取り出してYahooImageData構造体にデータを格納する
// ChromeDriverを起動してurlを取得
func (g *YahooImageData) fetchImageURLOrData() {
	// 視認して動作を確認する場合は--headlessの引数を外す
	// ChromeOptionsに逐次をオプションを指定して操作を変更できる
	// 参考：https://sites.google.com/a/chromium.org/chromedriver/
	driver := agouti.ChromeDriver(
		agouti.ChromeOptions("args", []string{
			"--headless",             // headlessモードの指定
			"--window-size=1280,800", // ウィンドウサイズの指定
		}),
		agouti.Debug,
	)
	if err := driver.Start(); err != nil {
		log.Fatalf("Failed to start driver:%v", err)
	}
	capabilities := agouti.NewCapabilities().Without("javascriptEnabled")
	page, err := driver.NewPage(agouti.Desired(capabilities))

	defer driver.Stop()
	if err != nil {
		log.Fatalf("Failed to open page:%v", err)
	}

	// yahoo画像検索URL
	url := "https://search.yahoo.co.jp/image/search?ei=UTF-8&fr=sfp_as&aq=-1&oq=&ts=3243&p=" + g.StockImage.KeyWord
	if err := page.Navigate(url); err != nil {
		log.Fatalf("Failed to navigate:%v", err)
	}

	body := page.Find("body")

	// ページネーションに切り替えるリンク
	body.FindByClass("defaultMode").Click()
	index := 2
	for {
		text := strconv.Itoa(index)
		hasText, _ := body.FindByLink(text).Enabled()
		if !hasText {
			break
		}

		body.FindByLink(text).Click()
		index += 1

		content, _ := page.HTML()
		reader := strings.NewReader(content)
		doc, _ := goquery.NewDocumentFromReader(reader)
		doc.Find(".gridmodule").Each(func(_ int, s *goquery.Selection) {
			url, _ := s.Find("img").Attr("src")
			g.URL = append(g.URL, url)
		})
	}
}

func (g *YahooImageData) fetchCreateImage() {
	if _, err := os.Stat(g.StockImage.FolderName); err != nil {
		os.Mkdir(g.StockImage.FolderName, 0777)
	}
	for _, url := range g.URL {
		// URL名だとfile名が長くなってしまいエラーになるので
		// SHA1に変換して重複した画像かどうか判断する
		fileName := octopus_stock.CreateSHA1(url)

		response, err := http.Get(url)
		if err != nil {
			fmt.Println(err)
			// panic(err)
		}
		defer response.Body.Close()

		if err != nil {
			// 画像フォーマットではない場合はエラーが発生する
			fmt.Println(err)
			return
		}

		filePath := g.StockImage.FolderName + "/" + fileName
		file, err := os.Create(filePath)
		if err != nil {
			panic(err)
		}

		io.Copy(file, response.Body)

		formatType, err := octopus_stock.ImageType(filePath)
		if err := os.Rename(filePath, filePath+"."+formatType); err != nil {
			fmt.Println(err)
		}

		g.StockImage.Count += 1

		defer file.Close()
	}
}
