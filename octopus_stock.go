package octopus_stock

type StockImage struct {
	FolderName string // ダウンロードするフォルダ名を指定する
	KeyWord    string // 取得してくるキーワードを設定する
	Count      int    // 保存した画像の数
}
