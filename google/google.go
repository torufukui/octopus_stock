package google

import (
	"encoding/base64"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/sclevine/agouti"
	"gitlab.com/torufukui/octopus_stock"
)

// Google画像検索でfetchしてきたデータ情報を格納する構造体
type GoogleImageData struct {
	URL                       []string // 取得先のURL
	EncodeStrings             []string // Base64でエンコードされたデータを保存する
	*octopus_stock.StockImage          // 画像関連の情報を保持する
}

// New creates a new GoogleImageData
func New(o *octopus_stock.StockImage) *GoogleImageData {
	return &GoogleImageData{
		StockImage: o,
	}
}

func (g *GoogleImageData) ImageStock() {
	g.fetchImageURLOrData()
	g.fetchCreateImage()
	// g.decodeToCreateImage()
}

// Google画像検索で指定したキーワードを元に画像データのURLを取り出してGoogleImageData構造体にデータを格納する
// 文字数の長さが1000以上ならば、base64エンコードされた文字列なのでGoogleImageData.EncodeStringに格納
// 1000以下ならGoogleImageData.URLに格納
// ChromeDriverを起動してurlを取得
func (g *GoogleImageData) fetchImageURLOrData() {
	// 視認して動作を確認する場合は--headlessの引数を外す
	// ChromeOptionsに逐次をオプションを指定して操作を変更できる
	// 参考：https://sites.google.com/a/chromium.org/chromedriver/
	driver := agouti.ChromeDriver(
		agouti.ChromeOptions("args", []string{
			"--headless",             // headlessモードの指定
			"--window-size=1280,800", // ウィンドウサイズの指定
		}),
		agouti.Debug,
	)
	if err := driver.Start(); err != nil {
		log.Fatalf("Failed to start driver:%v", err)
	}
	page, err := driver.NewPage()

	defer driver.Stop()
	if err != nil {
		log.Fatalf("Failed to open page:%v", err)
	}

	// google画像検索URL
	url := "https://www.google.com/search?hl=jp&q=" + g.StockImage.KeyWord + "&btnG=Google+Search&tbs=0&safe=off&tbm=isch"
	if err := page.Navigate(url); err != nil {
		log.Fatalf("Failed to navigate:%v", err)
	}

	body := page.Find("body")
	// スクロール自動で移動する
	// ここの数値を調整すること取得できる画像数を変更できる
	body.ScrollFinger(0, 10000)

	content, _ := page.HTML()

	reader := strings.NewReader(content)
	doc, _ := goquery.NewDocumentFromReader(reader)

	doc.Find("img.rg_ic.rg_i").Each(func(_ int, s *goquery.Selection) {
		url, _ := s.Attr("src")
		if len(url) > 1000 {
			g.EncodeStrings = append(g.EncodeStrings, url)
		} else {
			g.URL = append(g.URL, url)
		}
	})
}

// Base64された文字列をデコードして画像ファイルに変換する
func (g *GoogleImageData) decodeToCreateImage() {
	for _, encodingString := range g.EncodeStrings {
		name := strconv.Itoa(g.StockImage.Count)

		data, _ := base64.StdEncoding.DecodeString(encodingString) //[]byte
		fmt.Println(data)
		filePath := g.StockImage.FolderName + "/" + name
		file, _ := os.Create(filePath)
		defer file.Close()

		if err := os.Rename(filePath, filePath+".jpg"); err != nil {
			fmt.Println(err)
		}
		file.Write(data)
	}
}

func (g *GoogleImageData) fetchCreateImage() {
	if _, err := os.Stat(g.StockImage.FolderName); err != nil {
		os.Mkdir(g.StockImage.FolderName, 0777)
	}
	for _, url := range g.URL {
		// URL名だとfile名が長くなってしまいエラーになるので
		// SHA1に変換して重複した画像かどうか判断する
		fileName := octopus_stock.CreateSHA1(url)

		response, err := http.Get(url)
		if err != nil {
			fmt.Println(err)
			// panic(err)
		}
		defer response.Body.Close()

		if err != nil {
			// 画像フォーマットではない場合はエラーが発生する
			fmt.Println(err)
			return
		}

		filePath := g.StockImage.FolderName + "/" + fileName
		file, err := os.Create(filePath)
		if err != nil {
			panic(err)
		}

		io.Copy(file, response.Body)

		formatType, err := octopus_stock.ImageType(filePath)
		if err := os.Rename(filePath, filePath+"."+formatType); err != nil {
			fmt.Println(err)
		}

		g.StockImage.Count += 1

		defer file.Close()
	}
}
