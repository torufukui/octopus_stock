# octopus_stock


## dependices

```
$ brew cask install chromedriver
$ go get github.com/PuerkitoBio/goquery
$ go get github.com/sclevine/agouti
```

## install

```
$ go get gitlab.com/torufukui/octopus_stock
```

## How to use

```
  package main

  import (
    "gitlab.com/torufukui/octopus_stock"
    "gitlab.com/torufukui/octopus_stock/google"
    "gitlab.com/torufukui/octopus_stock/yahoo"
    "gitlab.com/torufukui/octopus_stock/bing"
  )

  func main() {
    stock_image := &octopus_stock.StockImage{
      FolderName: "Folder name",  // Specify the folder to download
      KeyWord:    "keyword name", // Specify search keywords
    }

    google := google.New(stock_image)
    google.ImageStock()

    yahoo := yahoo.New(stock_image)
    yahoo.ImageStock()

    bing := bing.New(stock_image)
    bing.ImageStock()
  }

```
