package octopus_stock

import (
	"crypto/sha1"
	"fmt"
	"image"
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
	"os"
)

// SHA1に変換して16進数で表示する
func CreateSHA1(name string) string {
	h := sha1.New()
	h.Write([]byte(name))
	bs := h.Sum(nil)
	return fmt.Sprintf("%x", bs)
}

// 画像フォーマットを出力する
func ImageType(fileName string) (string, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return "", err
	}
	defer file.Close()

	_, format, err := image.DecodeConfig(file)
	if err != nil {
		// 画像フォーマットではない場合はエラーが発生する
		fmt.Println(err)
		return "", err
	}
	return format, nil
}
