package bing

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/sclevine/agouti"
	"gitlab.com/torufukui/octopus_stock"
)

// Bing画像検索でfetchしてきたデータ情報を格納する構造体
type BingImageData struct {
	URL                       []string // 取得先のURL
	EncodeStrings             []string // Base64でエンコードされたデータを保存する
	*octopus_stock.StockImage          // 画像関連の情報を保持する
}

// New creates a new BingImageData
func New(o *octopus_stock.StockImage) *BingImageData {
	return &BingImageData{
		StockImage: o,
	}
}

func (g *BingImageData) ImageStock() {
	g.fetchImageURLOrData()
	g.fetchCreateImage()
}

// Bing画像検索で指定したキーワードを元に画像データのURLを取り出してBingImageData構造体にデータを格納する
// ChromeDriverを起動してurlを取得
func (g *BingImageData) fetchImageURLOrData() {
	// 視認して動作を確認する場合は--headlessの引数を外す
	// ChromeOptionsに逐次をオプションを指定して操作を変更できる
	// 参考：https://sites.google.com/a/chromium.org/chromedriver/
	driver := agouti.ChromeDriver(
		agouti.ChromeOptions("args", []string{
			"--headless",             // headlessモードの指定
			"--window-size=1280,800", // ウィンドウサイズの指定
		}),
		agouti.Debug,
	)
	if err := driver.Start(); err != nil {
		log.Fatalf("Failed to start driver:%v", err)
	}
	page, err := driver.NewPage()

	defer driver.Stop()
	if err != nil {
		log.Fatalf("Failed to open page:%v", err)
	}

	// bing画像検索URL
	url := "https://www.bing.com/images/search?q=" + g.StockImage.KeyWord + "&qs=n&form=QBLH&scope=images"
	if err := page.Navigate(url); err != nil {
		log.Fatalf("Failed to navigate:%v", err)
	}

	body := page.Find("body")
	// スクロール自動で移動する
	// ここの数値を調整すること取得できる画像数を変更できる
	body.ScrollFinger(0, 10000)

	content, _ := page.HTML()

	reader := strings.NewReader(content)
	doc, _ := goquery.NewDocumentFromReader(reader)
	doc.Find("div.img_cont").Each(func(_ int, s *goquery.Selection) {
		url, _ := s.Find("img").Attr("src")
		if len(url) > 1000 {
			g.EncodeStrings = append(g.EncodeStrings, url)
		} else {
			g.URL = append(g.URL, url)
		}
	})
}

func (g *BingImageData) fetchCreateImage() {
	if _, err := os.Stat(g.StockImage.FolderName); err != nil {
		os.Mkdir(g.StockImage.FolderName, 0777)
	}
	for _, url := range g.URL {
		// URL名だとfile名が長くなってしまいエラーになるので
		// SHA1に変換して重複した画像かどうか判断する
		fileName := octopus_stock.CreateSHA1(url)

		response, err := http.Get(url)
		if err != nil {
			fmt.Println(err)
			// panic(err)
		}
		defer response.Body.Close()

		if err != nil {
			// 画像フォーマットではない場合はエラーが発生する
			fmt.Println(err)
			return
		}

		filePath := g.StockImage.FolderName + "/" + fileName
		file, err := os.Create(filePath)
		if err != nil {
			panic(err)
		}

		io.Copy(file, response.Body)

		formatType, err := octopus_stock.ImageType(filePath)
		if err := os.Rename(filePath, filePath+"."+formatType); err != nil {
			fmt.Println(err)
		}

		g.StockImage.Count += 1

		defer file.Close()
	}
}
